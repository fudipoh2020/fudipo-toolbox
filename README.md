# FUDIPO open source Toolbox
This repository contains an example model (for calculating the heating value of large CHPs) and a calculation graph (Node-RED) to automatically execute the model. This Toolbox serves as an example on how to integrate and deploy heterogeneous models with varying model runtime requirements in one scale able unified solution. Encapsulating models in RESTful APIs (Flask apps) and defining a input and output data structures enables the possibility to execute different models using Node-RED. Node-RED is used as a calculation graph, which means it handles the model input data selection and preparation, model execution, model output data postprocessing and storage to a database and data visualization using Node-RED's dashboard.

To get started visit the Wiki to see setup and installation instructions.


[![This project is funded by the European Commission H2020 Research and Innovation program under Grant Agreement nº 723523.](https://gitlab.com/fudipoh2020/fudipo-toolbox/-/raw/master/imgs/FUDIPO_banner.png)](https://ec.europa.eu/info/research-and-innovation_en)
