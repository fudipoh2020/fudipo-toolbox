﻿using System.Web;
using System.Web.Mvc;

namespace ME_Hugin_Cyclon
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
