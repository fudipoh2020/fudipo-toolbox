﻿#if X64
using size_t = System.UInt64;
using h_index_t = System.Int64;
#else
using size_t = System.UInt32;
using h_index_t = System.Int32;
#endif
#if H_DOUBLE
using h_number_t = System.Double;
#else
using h_number_t = System.Single;
#endif

using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HAPI;
using System.Text;
using Newtonsoft.Json.Linq;


namespace ME_Hugin_Cyclon.Controllers
{
    public class ValuesController : ApiController
    {

        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public JObject Post([FromBody]JObject o1)
        {
            ParseListener parseListener = new DefaultClassParseListener();
            Domain domain = new Domain("C:\\FUDIPO\\Hugin\\data_fullload_vALM.net", parseListener);
            domain.OpenLogFile(@"C:\FUDIPO\Hugin\data_fullload_vALM.log");
            domain.Triangulate(Domain.TriangulationMethod.H_TM_BEST_GREEDY);
            domain.Compile();
            domain.CloseLogFile();

            NodeList nodes = domain.GetNodes();
            bool hasUtilities = ContainsUtilities(nodes);

            // Parse the input message from post request into a Newtonsoft JSON object

            // Create new dataset to enter it into the Hugin domain
            DataSet myDataSet = new DataSet();
            
            // Create exactly one new row for data to be entered
            myDataSet.NewRow();

            // Enter data from JSON into the dataset
            uint index = 0;
            foreach (var x in o1)
            {
                myDataSet.NewColumn(x.Key);
                myDataSet.SetDataItem(0, index, (string)x.Value);
                index++;
            }

            // Console.WriteLine("Try to save myDataSet to csv file");
            myDataSet.SaveAsCSV("C:\\FUDIPO\\Hugin\\SavedDataSetFileFromPOSTJSON.csv", ',');

            // All cases in dataSetFile are written to the domain 
            // Console.WriteLine("Adding myDataSet to the domain.");
            domain.AddCases(myDataSet);

            // The first case with index 0 is entered as current evidence for inference/propagation 
            domain.EnterCase(0);

            // Propagate the model (inference)
            domain.Propagate(Domain.Equilibrium.H_EQUILIBRIUM_SUM,
                              Domain.EvidenceMode.H_EVIDENCE_MODE_NORMAL);
            
            // result will hold the results
            JObject result = new JObject();

            foreach (Node node in nodes)
            {
                System.Diagnostics.Debug.WriteLine("\n");
                System.Diagnostics.Debug.WriteLine(node.GetLabel() + " (" + node.GetName() + ")");
                //result[node.GetName()] = node.GetName();

                if (node is UtilityNode)
                {
                    UtilityNode uNode = (UtilityNode)node;
                    System.Diagnostics.Debug.WriteLine("  - Expected utility: "
                                  + ((UtilityNode)node).GetExpectedUtility());
                    result[uNode.GetName()] = uNode.GetExpectedUtility();
                }
                else if (node is DiscreteNode)
                {
                    DiscreteNode dNode = (DiscreteNode)node;

                    for (size_t i = 0, n = (uint)dNode.GetNumberOfStates(); i < n; i++)
                    {
                        System.Diagnostics.Debug.WriteLine("  - " + dNode.GetStateLabel(i)
                                  + " " + dNode.GetBelief(i));
                        result[dNode.GetName()+"_State"+i+dNode.GetStateLabel(i)] = dNode.GetBelief(i);

                        if (hasUtilities)
                        {
                            System.Diagnostics.Debug.WriteLine(" (" + dNode.GetExpectedUtility(i) + ")");
                            result[node.GetName()+"_Utility"] = dNode.GetExpectedUtility(i);
                        }
                        else
                            System.Diagnostics.Debug.WriteLine("\n");

                    }
                }
                else if (node is FunctionNode)
                {
                    FunctionNode fNode = (FunctionNode)node;
                    try
                    {
                        double value = fNode.GetValue();
                        System.Diagnostics.Debug.WriteLine("  - Function value : " + value);
                        result[fNode.GetName()] = value;
                    }
                    catch (ExceptionHugin)
                    {
                        System.Diagnostics.Debug.WriteLine("  - Function value : N/A");
                    }
                }
                else
                {
                    ContinuousChanceNode ccNode = (ContinuousChanceNode)node;

                    System.Diagnostics.Debug.WriteLine("  - Mean : " + ccNode.GetMean());
                    System.Diagnostics.Debug.WriteLine("  - SD   : " + Math.Sqrt(ccNode.GetVariance()));
                }
            }
            domain.Delete();
            return result;
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
        
        public static void LAP(String netName, String caseName)
        {
            try
            {
                ParseListener parseListener = new DefaultClassParseListener();
                Domain domain = new Domain(@"data_fullload_vALM.net", parseListener);
                domain.OpenLogFile(netName + ".log");
                domain.Triangulate(Domain.TriangulationMethod.H_TM_BEST_GREEDY);
                domain.Compile();
                domain.CloseLogFile();

                bool hasUtilities = ContainsUtilities(domain.GetNodes());

                if (!hasUtilities)
                    System.Diagnostics.Debug.WriteLine("Prior beliefs:");
                else
                {
                    domain.UpdatePolicies();
                    System.Diagnostics.Debug.WriteLine("Overall expected utility: "
                                + domain.GetExpectedUtility());
                    System.Diagnostics.Debug.WriteLine("\n");
                    System.Diagnostics.Debug.WriteLine("Prior beliefs (and expected utilities):");
                }
                PrintBeliefsAndUtilities(domain);

                if (caseName != null)
                {
                    domain.ParseCase(caseName, parseListener);

                    System.Diagnostics.Debug.WriteLine("\n");
                    System.Diagnostics.Debug.WriteLine("\n");
                    System.Diagnostics.Debug.WriteLine("Propagating the evidence specified in \""
                                + caseName + "\"");

                    domain.Propagate(Domain.Equilibrium.H_EQUILIBRIUM_SUM,
                              Domain.EvidenceMode.H_EVIDENCE_MODE_NORMAL);

                    System.Diagnostics.Debug.WriteLine("\n");
                    System.Diagnostics.Debug.WriteLine("P(evidence) = "
                                + domain.GetNormalizationConstant());
                    System.Diagnostics.Debug.WriteLine("\n");

                    if (!hasUtilities)
                        System.Diagnostics.Debug.WriteLine("Updated beliefs:");
                    else
                    {
                        domain.UpdatePolicies();
                        System.Diagnostics.Debug.WriteLine("Overall expected utility: "
                                + domain.GetExpectedUtility());
                        System.Diagnostics.Debug.WriteLine("\n");
                        System.Diagnostics.Debug.WriteLine("Updated beliefs (and expected utilities):");
                    }
                    PrintBeliefsAndUtilities(domain);
                }

                domain.Delete();
            }
            catch (ExceptionHugin e)
            {
                System.Diagnostics.Debug.WriteLine("Exception caught: " + e.Message);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("General exception: " + e.Message);
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
        }

        public static void PrintBeliefsAndUtilities(Domain domain)
        {
            NodeList nodes = domain.GetNodes();
            bool hasUtilities = ContainsUtilities(nodes);
            foreach (Node node in nodes)
            {
                System.Diagnostics.Debug.WriteLine("\n");
                System.Diagnostics.Debug.WriteLine(node.GetLabel() + " (" + node.GetName() + ")");

                if (node is UtilityNode)
                    System.Diagnostics.Debug.WriteLine("  - Expected utility: "
                                + ((UtilityNode)node).GetExpectedUtility());
                else if (node is DiscreteNode)
                {
                    DiscreteNode dNode = (DiscreteNode)node;

                    for (size_t i = 0, n = (uint)dNode.GetNumberOfStates(); i < n; i++)
                    {
                        System.Diagnostics.Debug.WriteLine("  - " + dNode.GetStateLabel(i)
                                  + " " + dNode.GetBelief(i));
                        if (hasUtilities)
                            System.Diagnostics.Debug.WriteLine(" (" + dNode.GetExpectedUtility(i) + ")");
                        else
                            System.Diagnostics.Debug.WriteLine("\n");
                    }
                }
                else if (node is FunctionNode)
                {
                    FunctionNode fNode = (FunctionNode)node;
                    try
                    {
                        double value = fNode.GetValue();
                        System.Diagnostics.Debug.WriteLine("  - Function value : " + value);
                    }
                    catch (ExceptionHugin)
                    {
                        System.Diagnostics.Debug.WriteLine("  - Function value : N/A");
                    }
                }
                else
                {
                    ContinuousChanceNode ccNode = (ContinuousChanceNode)node;

                    System.Diagnostics.Debug.WriteLine("  - Mean : " + ccNode.GetMean());
                    System.Diagnostics.Debug.WriteLine("  - SD   : " + Math.Sqrt(ccNode.GetVariance()));
                }
            }
        }

        public static bool ContainsUtilities(NodeList list)
        {
            foreach (Node node in list)
            {
                if (node is UtilityNode)
                    return true;
            }

            return false;
        }
    }
}
