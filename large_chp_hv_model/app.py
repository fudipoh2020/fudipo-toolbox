from flask import Flask, Blueprint, request
from flask_restplus import Resource, Api, fields
from functools import wraps
from pyfmi import load_fmu
import json
import datetime
import numpy as np
import os
from logging.config import dictConfig

dictConfig({
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
    }},
    'handlers': {
        'wsgi': {
            'class': 'logging.StreamHandler',
            'formatter': 'default'
        },
        'custom_handler': {
            'class': 'logging.FileHandler',
            'formatter': 'default',
            'filename': r'myapp.log'
        }
    },
    'root': {
        'level': 'INFO',
        'handlers': ['wsgi', 'custom_handler']
    }
})

cur_dir = os.path.dirname(os.path.abspath(__file__))

global model
model = load_fmu(os.path.join(cur_dir,
                              'LargeScaleCHP_FMU_calc_HV.fmu'))
global my_options
my_options = model.simulate_options()
# model.setup_experiment()
# model.initialize()
model.reset()
model.simulate(final_time=0)
my_options['initialize'] = False

authorizations = {
    'apiKey': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'X-API-KEY'
    }
}

app = Flask(__name__)

blueprint = Blueprint('api', __name__, url_prefix='/api')
api = Api(blueprint, doc='/doc', version='0.1',
          title='Large Scale CHP FMU for calculating fuel heating value',
          description='FMU from Nathan Zimmerman',
          authorizations=authorizations)

app.register_blueprint(blueprint)

app.config['SWAGGER_UI_JSONEDITOR'] = True
app.config['RESTPLUS_VALIDATE'] = True


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None
        if 'X-API-KEY' in request.headers:
            token = request.headers['X-API-KEY']
        if not token:
            return {'message': 'Token is missing (X-API-KEY).'}, 403
        if token != '123':
            return {'message': 'Token is not valid.'}, 403
        return f(*args, **kwargs)

    return decorated


example_data = {
    "tag_keys": ["effect", "losses", "fuel_massFlow"],
    "tag_values": [[100, 15, 162], [160, 20, 230]],
    "time_values": [[0.0], [60]]
}

input_params = api.model('Input Parameters', {
    'ncp': fields.Integer(required=True,
                          description='Number of calculation points.',
                          default=10,
                          example=10),
    'reset': fields.Boolean(required=True,
                            description='True resets the model.',
                            default=False,
                            example=False),
    'sim_time': fields.Float(required=True,
                             description='The final time of the model '
                                         'execution in seconds',
                             default=10,
                             example=10),
    'selection_time': fields.DateTime(required=True,
                                      description='The timestamp from when '
                                                  'the data has been selected'),
    'Data': fields.Raw(required=True,
                       description='The input data for the FMU',
                       default=example_data,
                       example=example_data)
})


@api.route('/hello')
class Hello(Resource):
    @api.doc(security='apiKey')
    @token_required
    def get(self):
        app.logger.info('Got Request on /hello')
        return {'Hello': 'World'}


@api.route('/execute')
class Execute(Resource):
    @api.doc(security='apiKey')
    @api.expect(input_params, validate=True)
    @token_required
    def post(self):
        global model
        app.logger.info(
            f'{datetime.datetime.now().isoformat()} - Got request with the '
            f'following input: {json.dumps(api.payload)}')
        final_time = api.payload['sim_time']

        tmp = api.payload['Data']
        tag_keys = tmp['tag_keys']
        tag_values = np.array(
            tmp['tag_values'])  # ([tmp['tag_values'][0], tmp['tag_values'][1]])
        time_values = np.array(tmp[
                                   'time_values'])  # [tmp['time_values'][0], tmp['time_values'][1]])
        input_vals = np.hstack((time_values, tag_values))
        if api.payload['reset']:
            # model.reset()
            model = load_fmu(os.path.join(cur_dir,
                                          'LargeScaleCHP_FMU_calc_HV.fmu'))
            my_options['initialize'] = True
            # model.simulate(final_time=0)
            # model.setup_experiment()
            # model.initialize()
        else:
            my_options['initialize'] = False
        my_options['ncp'] = api.payload['ncp']
        res = model.simulate(input=(tag_keys, input_vals),
                             options=my_options,
                             start_time=model._last_accepted_time,
                             final_time=model._last_accepted_time + final_time
                             )

        parsed_res = {}
        for i, tag in enumerate(res.result_data.name):
            parsed_res[tag] = list(res[tag])

        return_object = {
            'selection_time': api.payload['selection_time'],
            'results': parsed_res
        }
        app.logger.info(
            f'{datetime.datetime.now().isoformat()} - Sending response: '
            f'{return_object}')
        # model.reset()
        return return_object


if __name__ == '__main__':
    app.run(port=5000, host='0.0.0.0')
